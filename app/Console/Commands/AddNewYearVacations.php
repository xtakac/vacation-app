<?php

namespace App\Console\Commands;

use App\Http\Interfaces\IVacationService;
use Illuminate\Console\Command;

class AddNewYearVacations extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'add:vacations';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Adds vacation days to employees on new year';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(IVacationService $service)
    {
        $service->addNewYearVacations();

        return Command::SUCCESS;
    }
}
