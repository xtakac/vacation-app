<?php

namespace App\Http\Services;

use App\Http\Interfaces\IVacationService;
use App\Mail\VacationStatusChangedMail;
use App\Models\User;
use App\Models\UserVacation;
use App\Models\Vacation;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

const COUNTRY_FREE_DAYS = 20;

class SlovakiaVacationService implements IVacationService
{
    public function processVacation(Vacation $vacation) : void
    {
        if ($vacation->status_id == 2) {         // if accepted
            $this->approveVacation($vacation);
        } elseif ($vacation->getOriginal('status_id') == 2) { // was accepted
            // TOOD -> add vacation back to user
        }

        $this->notifyUser($vacation);
    }

    public function addNewYearVacations()
    {
        foreach(User::all() as $user)
        {
            $vacationDays = COUNTRY_FREE_DAYS + ($user->has_child ? 5 : 0) + ($user->age() > 33 ? 5 : 0) + $user->bonus_vacation;

            UserVacation::create([
                'user_id' => $user->id,
                'days' => $vacationDays,
                'year' => now()->year
            ]);
        }
    }

    public function notifyUser(Vacation $vacation)
    {
        Mail::to($vacation->user->email)->send(new VacationStatusChangedMail($vacation));
    }

    public function approveVacation(Vacation $vacation)
    {
        $user = $vacation->user;

        $interval = (new \DateTime($vacation->end))->diff(new \DateTime($vacation->start))->format('%a') + 1;

        $totalVacationLeft = UserVacation::select(
            \DB::raw('sum(days) as sum')
        )->where('user_id', $user->id)
        ->groupBy('user_id')
        ->value('sum');

        if($totalVacationLeft < $interval) {
            throw new \Exception('User doenst have enough vacation!');
        }

        $this->deductVacaton($interval, $user->id);
    }

    private function deductVacaton($interval, $userId)
    {
        $oldestVacation = UserVacation::where('user_id', $userId)
            ->where('year', function ($query) use ($userId) {
                $query->from(\DB::raw('(SELECT * FROM user_vacations) AS v'))
                    ->where('v.user_id', $userId)
                    ->where('v.days', '>', 0)
                    ->selectRaw("MIN(`v`.`year`)");
            })->first();

        if(($oldestVacation->days - $interval) < 0) {
            $interval -= $oldestVacation->days;

            $oldestVacation->days -= $oldestVacation->days;
            $oldestVacation->save();

            return $this->deductVacaton($interval, $userId);
        }

        $oldestVacation->days -= $interval;
        $oldestVacation->save();

        return 1;
    }
}
