<?php

namespace App\Http\Services;

use App\Http\Interfaces\IVacationService;
use App\Models\Vacation;

class CzechiaVacationService implements IVacationService
{
    public function processVacation(Vacation $vacation)
    {

    }

    public function addNewYearVacations()
    {

    }

    public function approveVacation(Vacation $vacation)
    {

    }

    public function refuseVacation(Vacation $vacation)
    {

    }
}
