<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class VacationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'username' => $this->user->name,
            'user_id' => $this->user->id,
            'status_id' => $this->vacationStatus->id,
            'status' => $this->vacationStatus,
            'link' => route('vacations.show', ['vacation' => $this->id]),
            'start' => Carbon::parse($this->start)->toDateString(),
            'end' => Carbon::parse($this->end)->toDateString()
        ];
    }
}
