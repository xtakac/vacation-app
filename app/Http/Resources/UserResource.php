<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'url' => route('users.show', ['user' => $this->id]),
            'job_title' => $this->job_title,
            'birthdate' => $this->birthdate,
            'role' => $this->is_admin ? 'admin' : 'user',
            'is_admin' => $this->is_admin,
            'has_child' => $this->has_child,
            'bonus_vacation' => $this->bonus_vacation,
            'vacations' => VacationResource::collection($this->whenLoaded('vacations')),
            'userVacations' => UserVacationResource::collection($this->whenLoaded('userVacations')),
        ];
    }
}
