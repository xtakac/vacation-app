<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Interfaces\IVacationService;
use App\Http\Requests\VacationRequest;
use App\Http\Resources\VacationResource;
use App\Models\Vacation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Inertia\Inertia;

class VacationController extends Controller
{
    /**
     * @return \Inertia\Response
     */
    public function index()
    {
        $query = Vacation::query();

        if(!Auth::user()->isAdmin()) {
            $query->where('user_id', Auth::user()->id);
        }

        return Inertia::render('Vacations/List', [
            'vacations' => VacationResource::collection($query->with('vacationStatus')->paginate())
        ]);
    }

    /**
     * @return \Inertia\Response
     */
    public function create()
    {
        return Inertia::render('Vacations/CreateVacation');
    }

    /**
     * @param VacationRequest $request
     */
    public function store(VacationRequest $request)
    {
        $vacation = Vacation::create($request->validated());

        return Inertia::render('Vacations/EditVacation', [
            'vacation' => VacationResource::make($vacation)
        ])->with('success', 'Vacation request created');
    }

    /**
     * @param Vacation $vacation
     * @return \Inertia\Response
     */
    public function show(Vacation $vacation)
    {
        if(!Gate::allows('update-vacation', $vacation)) {
            abort(403);
        }

        $vacation->user;

        return Inertia::render('Vacations/EditVacation', [
            'vacation' => VacationResource::make($vacation)
        ]);
    }

    /**
     * @param Vacation $vacation
     */
    public function edit(Vacation $vacation)
    {
        //
    }

    /**
     * @param VacationRequest $request
     * @param Vacation $vacation
     * @param IVacationService $service
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(VacationRequest $request, Vacation $vacation, IVacationService $service)
    {
        \DB::beginTransaction();

        $vacation->fill($request->validated());

        if($vacation->isDirty('status_id')) {
            try {
                $service->approveVacation($vacation);
            } catch (\Exception $e) {
                \Log::info((array) $e);
                \DB::rollBack();

                return redirect()->back()->with('error', $e->getMessage());
            }
        }

        $vacation->save();

        \DB::commit();

        return redirect()->back()->with('success', 'Vacation updated!');
    }

    /**
     * @param Vacation $vacation
     */
    public function destroy(Vacation $vacation)
    {
        //
    }
}
