<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use Inertia\Inertia;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Inertia\Response
     */
    public function index()
    {
        return Inertia::render('Users/List', [
            'users' => UserResource::collection(User::paginate())
        ]);
    }

    /**
     * Display the specified resource.
     * @param User $user
     * @return \Inertia\Response
     */
    public function show(User $user)
    {
        $user->vacations;
        $user->userVacations;

        $vacations = \DB::query()->select(
                \DB::raw('sum(DATEDIFF(vacations.end, vacations.start)) as data'),
                \DB::raw('MONTH(vacations.start) as label')
            )->from('vacations')
            ->where('user_id', $user->id)
            ->where('status_id', 2)
            ->groupBy(['label'])
            ->orderBy('label')
            ->get();

        return Inertia::render('Users/EditUser', [
            'userDetail' => UserResource::make($user),
            'chartVacations' => $vacations
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UserRequest  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, User $user)
    {
        $user->update($request->validated());

        return redirect()->back()->with('success', 'User updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();

        return redirect()->route('users.index');
    }
}
