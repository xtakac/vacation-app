<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Interfaces;

use App\Models\Vacation;

/**
 *
 * @author jaro
 */
interface IVacationService
{
    /**
     * Processes vacation
     *
     * @param Vacation $vacation
     * @return mixed
     */
    public function processVacation(Vacation $vacation) : void;

    /**
     * @return mixed
     */
    public function addNewYearVacations();

    /**
     * @param Vacation $vacation
     * @return mixed
     */
    public function approveVacation(Vacation $vacation);
}
