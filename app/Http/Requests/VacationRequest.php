<?php

namespace App\Http\Requests;

use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;

class VacationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Gate::allows('update-vacation', $this->route('vacation'));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'description' => ['string', 'nullable'],
            'name' => ['required', 'string'],
            'status_id' => ['integer'],
            'start' => ['date'],
            'end' => ['date'],
            'user_id' => ['required', 'integer']
        ];
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        $this->merge([
            'start' => Carbon::parse($this->range['start']),
            'end' => Carbon::parse($this->range['end']),
        ]);
    }
}
