<?php

namespace App\Mail;

use App\Models\Vacation;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class VacationStatusChangedMail extends Mailable
{
    use Queueable, SerializesModels;

    public $vacation;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Vacation $vacation)
    {
        $this->vacation = $vacation;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Vacation status changed')
            ->text('Hello, vacaton ' . route('vacation.show', ['vacation' => $this->vacation->id]) . ' was changed');
    }
}
