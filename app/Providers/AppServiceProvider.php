<?php

namespace App\Providers;

use App\Http\Interfaces\IVacationService;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\ServiceProvider;
use Inertia\Inertia;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $vacationService = 'App\\Http\\Services\\' . ucfirst(config('app.country')) . 'VacationService';

        $this->app->singleton(IVacationService::class, function () use ($vacationService) {
            return new $vacationService;
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Inertia::share('flash', function () {
            return [
                'success' => Session::get('success'),
                'error' => Session::get('error'),
            ];
        });
    }
}
