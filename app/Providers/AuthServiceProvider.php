<?php

namespace App\Providers;

use App\Models\User;
use App\Models\Vacation;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('update-vacation', function (User $user, Vacation $vacation = null) {
            return $user->isAdmin() ? true : ($vacation ? ($user->id === $vacation->user_id) : true);
        });

        Gate::define('update-user', function (User $user) {
            return $user->isAdmin();
        });
    }
}
