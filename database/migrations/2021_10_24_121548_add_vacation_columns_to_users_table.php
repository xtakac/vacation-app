<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddVacationColumnsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->boolean('is_admin')->default(false);
            $table->string('job_title', 50)->nullable();
            $table->date('birthdate')->nullable();
            $table->boolean('has_child')->default(false);
            $table->integer('bonus_vacation')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('is_admin');
            $table->dropColumn('job_title');
            $table->dropColumn('birthdate');
            $table->dropColumn('has_child');
            $table->dropColumn('bonus_vacation');
        });
    }
}
