<?php

namespace Database\Seeders;

use App\Models\UserVacation;
use Illuminate\Database\Seeder;

class UserVacationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        UserVacation::insert([
            'user_id' => 2,
            'days' => 20,
            'year' => 2021
        ]);

        UserVacation::insert([
            'user_id' => 2,
            'days' => 5,
            'year' => 2020
        ]);
    }
}
