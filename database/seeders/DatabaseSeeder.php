<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(VacationStatusSeeder::class);
        $this->call(UserVacationSeeder::class);
        $this->call(VacationSeeder::class);
        $this->call(UserSeeder::class);
    }
}
