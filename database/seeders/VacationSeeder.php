<?php

namespace Database\Seeders;

use App\Models\Vacation;
use Illuminate\Database\Seeder;

class VacationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Vacation::insert([
            'user_id' => 2,
            'start' => '2021-10-07',
            'end' => '2021-10-10',
            'name' => 'Stahovanie',
            'description' => 'Stahovanie do noveho bytu',
            'status_id' => 2,
        ]);

        Vacation::insert([
            'user_id' => 2,
            'start' => '2021-09-05',
            'end' => '2021-09-10',
            'name' => 'Dovolenka',
            'description' => 'Dovolenka',
            'status_id' => 2,
        ]);

        Vacation::insert([
            'user_id' => 2,
            'start' => '2021-09-05',
            'end' => '2021-09-10',
            'name' => 'Dovolenka',
            'description' => 'Dovolenka',
            'status_id' => 2,
        ]);

        Vacation::insert([
            'user_id' => 2,
            'start' => '2021-08-01',
            'end' => '2021-08-03',
            'name' => 'Lekar',
            'description' => 'Lekar',
            'status_id' => 2,
        ]);

        Vacation::insert([
            'user_id' => 2,
            'start' => '2021-07-05',
            'end' => '2021-07-07',
            'name' => 'Lekar',
            'description' => 'Lekar',
            'status_id' => 2,
        ]);

        Vacation::insert([
            'user_id' => 2,
            'start' => '2021-07-05',
            'end' => '2021-07-07',
            'name' => 'Lekar',
            'description' => 'Lekar',
            'status_id' => 3,
        ]);

        Vacation::insert([
            'user_id' => 2,
            'start' => '2021-07-05',
            'end' => '2021-07-07',
            'name' => 'Lekar',
            'description' => 'Lekar',
            'status_id' => 1,
        ]);
    }
}
