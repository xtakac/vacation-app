<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'admin@admin.test',
            'password' => Hash::make('aaaaaaaa'),
            'is_admin' => 1,
        ]);

        DB::table('users')->insert([
            'name' => 'employee',
            'email' => 'employee@employee.test',
            'password' => Hash::make('aaaaaaaa'),
            'birthdate' => Carbon::createFromFormat('Y-m-d', '2021-10-21'),
            'bonus_vacation' => 0,
            'job_title' => 'Employee',
        ]);

    }
}
