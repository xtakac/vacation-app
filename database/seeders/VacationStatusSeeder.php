<?php

namespace Database\Seeders;

use App\Models\VacationStatus;
use Illuminate\Database\Seeder;

class VacationStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        VacationStatus::insert(['name'=>'New', 'color' =>'grey']);

        VacationStatus::insert(['name'=>'Accepted', 'color' =>'green']);

        VacationStatus::insert(['name'=>'Rejected', 'color' =>'red']);
    }
}
